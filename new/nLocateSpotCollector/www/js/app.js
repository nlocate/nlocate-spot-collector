// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
nLocateSpots = angular.module('starter', ['ionic',  'ngStorage']);

nLocateSpots.run(function($ionicPlatform, $rootScope, $localStorage, $location, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    document.addEventListener("deviceready", onDeviceReady, false);
    // PhoneGap is ready
    function onDeviceReady() {

      navigator.geolocation.getCurrentPosition(afterPositionAcquired, onError, {enableHighAccuracy: true});
      function afterPositionAcquired(position) {
        $rootScope.latitude = position.coords.latitude;
        $rootScope.spots.latitude = position.coords.latitude;
        $rootScope.longitude = position.coords.longitude;
        $rootScope.spots.longitude = position.coords.longitude;
        //alert('yaha');
        $rootScope.setPosition(position.coords.latitude, position.coords.longitude);
        //$rootScope.locationAayo = true;

        if(navigator.connection.type != 'none') {
            $rootScope.locationAayo = true;
        }
        else{
            $rootScope.locationAayo = false;
        }
      }

      function onError(error) {
        alert('Please check your location settings and make sure they are turned on!');
      }
    }
  });


  //$rootScope.spotsLength = $localStorage.spots.length;
  $rootScope.showOptions = false;
  $rootScope.loggedIn = $localStorage.loggedIn;
  $rootScope.showOptionList = function(){
    if($rootScope.showOptions == false)
    {
      //$rootScope.$apply();
      $rootScope.showOptions = true;}
    else if( $rootScope.showOptions == true){
      $rootScope.showOptions = false;
    }
  }

  $rootScope.logout = function(){
    var confirmCancel = $ionicPopup.confirm({
      title: 'Logout!',
      template: "Sure you want to logout?",
      cancelText: 'Nope!',
      cancelType: 'button-positive',
      okText: "Yes",
      okType: 'button-assertive'

    });
    confirmCancel.then(function(response){
      if (response) {
        $localStorage.loggedIn = false;
        delete $localStorage.username;
        delete $localStorage.password;
        $rootScope.loggedIn = false;
        //$rootScope.$apply();
        $rootScope.showOptions = false;
        $location.path('/form');
        //$rootScope.updateVars();
        //$state.go('form', {}, {reload: true});
      }else{
        // don't delete
      }
    });

  }

  $rootScope.changeState = function(){
    $rootScope.showFullMap = false;
    $rootScope.showOptions = false;
    $rootScope.selectedAll = false;
  }


});

nLocateSpots.config(function($httpProvider, $locationProvider) {

  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  //$locationProvider.html5Mode(false).hashPrefix('!');

  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];

  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj) {
      value = obj[name];

      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };
});


