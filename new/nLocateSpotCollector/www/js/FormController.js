
FormController = function($location, $scope, $state, $localStorage, $ionicModal, $rootScope, $filter, $ionicPopup,$http, $window, $ionicNavBarDelegate,$timeout ) {

    document.addEventListener("deviceready", onDeviceReady, false);
    // PhoneGap is ready
    function onDeviceReady() {
        $rootScope.getCurrentLocation();
    }

    if(!$rootScope.spots){
        $rootScope.spots= {};
    }

    if(!$localStorage.spots) {
        $localStorage.spots = [];
    }


    $scope.showFullMap = false;
    $scope.showMap = function(){

        $scope.showFullMap = true;
    }

    $scope.hideMap = function(){
        $scope.showFullMap = false;
    }


    $rootScope.categories = ['Food and Drinks','Shopping','Health','Travel','Finance','Entertainment','Education', 'More'];

    $rootScope.setPosition = function(lat,long){

        $scope.map = {
            center: {latitude: lat, longitude: long},
            marker: {latitude: $scope.latitude, longitude: $scope.longitude},
            zoom: 19,
            pan: 'false',
            draggable: 'true',
            icon: "img/marker-d.png",
            options : {
                zoomControl : true,
                mapTypeControl: false,
                streetViewControl: false,
                scrollwheel: true,
                zoomControlOptions: {

                    position: google.maps.ControlPosition.LEFT_CENTER
                }
            },
            markerOptions: {

                zIndex: 99,
                optimized: false
            },
            events:{
                dragend: function (marker) {
                    $rootScope.$apply(function () {
                        //$scope.latitude = marker.position.lat();
                        $scope.latitude = $scope.map.center.latitude;
                        $rootScope.spots.latitude = $scope.map.center.latitude;
                        //$scope.longitude = marker.position.lng();
                        $scope.longitude = $scope.map.center.longitude;
                        $rootScope.spots.longitude = $scope.map.center.longitude;

                    });
                }
            }
        };
        $scope.fill= {
            color: '#00528c',
            opacity: 0.5
        }
        $scope.stroke = {
            color: '#00528c',
            weight: 1,
            opacity: 1
        }

    }

    $rootScope.getCurrentLocation = function() {

        navigator.geolocation.getCurrentPosition(afterPositionAcquired, onError, {enableHighAccuracy: true});
        function afterPositionAcquired(position) {
            $scope.latitude = position.coords.latitude;
            $rootScope.spots.latitude = position.coords.latitude;
            $scope.longitude = position.coords.longitude;
            $rootScope.spots.longitude = position.coords.longitude;

        }

        function onError(error) {
            alert('Please check your location settings and make sure they are turned on!');
        }
    };



//for camera
    $scope.app = {
        // Application Constructor
        initialize: function () {
            this.bindEvents();
            var imageInfo = [];

        },

        bindEvents: function () {

            document.addEventListener('deviceready', this.onDeviceReady, false);

        },

        onDeviceReady: function () {
            app.receivedEvent('deviceready');
        },
        // Update DOM on a Received Event
        receivedEvent: function (id) {
            var parentElement = document.getElementById(id);
            var listeningElement = parentElement.querySelector('.listening');
            var receivedElement = parentElement.querySelector('.received');

            listeningElement.setAttribute('style', 'display:none;');
            receivedElement.setAttribute('style', 'display:block;');

            console.log('Received Event: ' + id);
        },
        clear: function () {
            window.localStorage.clear();
            alert("All cleared");
        },

        takePicture: function () {

            function afterImageUriAcquired(imageData) {
                $scope.$apply();
                var dataDir;

                var name = imageData.substr(imageData.lastIndexOf('/') + 1);
                var gotFileEntry = function(fileEntry) {
                    var gotFileSystem = function(fileSystem) {
                        fileSystem.root.getDirectory("nLocate", {
                            create : true
                        }, function(dataDir) {
                            // copy the file
                            fileEntry.moveTo(dataDir, name, moveSuccess, fsFail);
                        }, dirFail);
                    };

                    var moveSuccess = function(fileSystem){
                        var fileURL = fileSystem.nativeURL;
                        if(!$rootScope.spots.image){
                            $rootScope.spots.image = [];
                        }

                        var imageDet = {
                            'name': fileURL,
                            'uploadingImage': false,
                            'uploadedImage': false,
                            'uploadError' : false
                        };
                        $rootScope.spots.image.push(imageDet);
                        $scope.$apply();
                    };
                    // get file system to copy or move image file to
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem,
                        fsFail);
                };
                // resolve file system for image
                window.resolveLocalFileSystemURL(imageData, gotFileEntry, fsFail);



                // file system fail
                var fsFail = function(error) {
                    alert("failed with error code: " + error.code);

                };

                var dirFail = function(error) {
                    alert("Directory error code: " + error.code);

                };

            }

            navigator.camera.getPicture(afterImageUriAcquired, function (message) {
                    alert(message);
                },
                {
                    quality: 90,
                    destinationType: Camera.DestinationType.FILE_URI,
                    saveToPhotoAlbum: true,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    correctOrientation: true
                });
        }


    };


    $scope.validateForm = function(name, address, location){

        if(!name || !address || !location) {
            if (!name) {
                $scope.emptyName = true;
            }
            if (!address) {
                $scope.emptyAddress = true;
            }
            if (!location) {
                $scope.emptyLatlng = true;
            }
        }

        else{
            $scope.emptyName = false;
            $scope.emptyAddress = false;
            $scope.emptyLatlng = false;
            $state.go('form2');
        }

    }
    //saving spots
        $scope.master ={};
        $scope.saveData = function(spots){
            spots.syncStatus = false;
            spots.logDetails = false;
            console.log(spots);
        $localStorage.spots.push(spots);
        var alertPopup = $ionicPopup.alert({
         title: 'Spot added!',
         template: spots.name+ ' has been added successfully.'
        });
        alertPopup.then(function(res) {
         $rootScope.spots = angular.copy($scope.master);
            //$scope.$apply();
         $state.go('form');
         });
    }

}


nLocateSpots.controller('FormController',FormController);

