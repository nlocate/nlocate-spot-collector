
SyncProcessController = function($location, $scope, $state, $localStorage, $ionicModal, $rootScope,  $filter, $ionicPopup,$http, $window, $ionicNavBarDelegate,$timeout ) {

    $scope.loggedIn = $localStorage.loggedIn;
    $rootScope.startSync = function(){
        if(!$localStorage.loggedIn){
            $scope.showPopup();
        }
        else if ($localStorage.loggedIn){
            $scope.prepareList();
        }
    }

    $scope.freshList = $localStorage.spots;
    $scope.syncingList = $filter('filter')($scope.freshList, {Selected: true});

    $scope.showPopup = function() {
        $scope.data = {}
        $scope.setDefault = function () {
            console.log('Default set', arguments);
            $scope.$onClose({ test: 'hello' });
        };

        $scope.user = {};
        $ionicPopup.show({
            template: '',
            title: 'Login Now',
            scope: $scope,
            template: '<div style="color: red;" ng-if="loginError">Invalid username or password. Try again.</div> <input type="text" ng-model="user.name" placeholder=" Username"><br/><input type="password" ng-model="user.password" placeholder=" Password">',
            buttons: [
                {
                    text: 'Cancel', onTap: function(e) { return 'close';  }
                },
                { text: 'Login', type: 'button-positive', onTap: function(e) { return 'login'; } },

            ]
        }).then(function(res) {
            console.log('Tapped!', res);
            if(res == 'close'){
                $state.go('sync');
            }
            else if(res == 'login'){
                //$scope.loggedIn = true;
                //$scope.$apply();
                $scope.login($scope.user.name, $scope.user.password);
            }
        }, function(err) {
            console.log('Err:', err);
        }, function(msg) {
            console.log('message:', msg);
        });
    };

    //login
    $scope.login = function(username, password) {

        $scope.loginApi = $scope.apiAddress+"/login/";

        var load = {username:username, password:password};
        var request = {
            method : 'POST',
            url : $scope.loginApi,
            data : load
        };
        // $http returns a promise, which has a then function, which also returns a promise
        $http.post(request.url, load ).
            success(function(response) {
                console.log(response);
                if(response.ok == true){
                    $scope.loginError = false;
                    $localStorage.loggedIn = true;
                    $localStorage.username = username;
                    $localStorage.password = password;
                    //$scope.syncData();
                    //$rootScope.updateSyncProgress();
                    $rootScope.prepareList();
                }
                else{
                    $scope.loginError = true;
                    $scope.loggedIn = false;
                    $scope.showPopup();
                }
            }).
            error(function(response) {
                console.log(response);
            });
        //$state.go('syncProgress');
    }

    $rootScope.prepareList = function(){
        $rootScope.syncData();
    }

    $rootScope.syncData = function(){
        //$rootScope.updateSyncProgress();
        $scope.username = $localStorage.username;
        $scope.password = $localStorage.password;

        $state.go('syncProgress', {}, { reload: true });


        for(i =0; i<$scope.syncList.length; i++) {
            $scope.syncList[i].errMsg = false;
            $scope.spotApi = $scope.apiAddress+"/add-spot/";
            //console.log($scope.syncList[i]);
            var spot = {username: $scope.username, password: $scope.password, index: i, data:$scope.syncList[i] };
            var request = {
                method : 'POST',
                url : $scope.spotApi,
                data : spot
            };
            // $http returns a promise, which has a then function, which also returns a promise

            if($scope.syncList[i].Selected == true) {
                $scope.callTimeOut(i);
                $http.post(request.url, spot).
                    success(function (response) {
                        console.log(response);
                        if (response.ok == true) {
                            if (!$scope.syncList[response.index].image) {
                                $localStorage.spots[response.index].syncStatus = true;
                                $scope.syncList[response.index].syncStatus = true;
                                $timeout.cancel($scope.checkTimeout);
                            }
                            else {
                                for (j = 0; j < $scope.syncList[response.index].image.length; j++) {
                                    $scope.uploadPhoto($scope.syncList[response.index].image[j], response.index, response.id);
                                }
                            }

                        }
                        else if(response.ok == false) {
                            $scope.syncList[response.index].errMsg = true;
                            $rootScope.errMsg = "Something went wrong with this spot. Please check once and try again!";
                            //$scope.$apply();
                        }

                    }).
                    error(function (response) {
                        console.log(response);
                    });
            }

        }
    }

    $scope.callTimeOut = function(i){
        $scope.checkTimeout = $timeout(function(){
            if($scope.syncList[i].syncStatus == false && $scope.syncList[i].errMsg == false ){
                $scope.syncList[i].errMsg = true;
                $rootScope.errMsg = "Something went wrong with this spot. Please check once and try again!";
            }

        }, 60000, true, i);
    }

    $scope.imageAPI = $scope.apiAddress+"/image/";
    $scope.uploadPhoto = function(imageURI, i, id) {
        //console.log(imageURI);
        var options = new FileUploadOptions();
        options.fileKey = "image";
        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";

        var params = new Object();
        params.username = $scope.username;
        params.password = $scope.password;
        params.index = i;
        params.id = id;
        options.params = params;
        options.chunkedMode = false;

        var ft = new FileTransfer();
        ft.upload(imageURI, $scope.imageAPI, win, fail, options);
    }

    function win(r) {
        //console.log("Code = " + r.responseCode);
        //console.log("Response = " + r.response);
        //console.log("Sent =  image " + i +"ko "+ r.bytesSent);
        //alert(r.response);
        console.log(i + ' uploaded');
        $scope.changeStatus(r.response);

    }

    function fail(error) {
        console.log("An error has occurred: Code = " + error.code);
    }

    $scope.changeStatus = function(response){
        response = JSON.parse(response);
        console.log(response);
        console.log(response.index_number);
        console.log($localStorage.spots[response.index_number].syncStatus);
        $localStorage.spots[response.index_number].syncStatus = true;
        $scope.syncList[response.index_number].syncStatus = true;
        $scope.$apply();
        console.log($localStorage.spots[response.index_number].syncStatus);
    }

    $scope.cancelSync = function(){
        var confirmCancel = $ionicPopup.confirm({
            title: 'DANGER !',
            template: "Your spots are being uploaded. Sure you want to cancel?",
            cancelText: 'Nope!',
            cancelType: 'button-positive',
            okText: "Yes",
            okType: 'button-assertive'

        });
        confirmCancel.then(function(response){
            if (response) {
                $state.go('sync', {}, {reload: true});
            }else{
                // don't delete
            }
        });

    }
}

nLocateSpots.controller('SyncProcessController',SyncProcessController);