ListController = function($location, $scope, $state, $localStorage, $ionicModal, $rootScope,  $filter, $ionicPopup, $http, $window, $ionicNavBarDelegate,$timeout ) {

    //console.log("reload list");
    $scope.allList = $localStorage.spots;
    $scope.syncList = $filter('filter')($scope.allList, {syncStatus: false});
    $localStorage.spots = $scope.syncList;


    $scope.delete = function(item){
        var confirmDel = $ionicPopup.confirm({
            title: 'DANGER !',
            template: "Sure you want to delete this entry?",
            cancelText: 'Nope!',
            cancelType: 'button-positive',
            okText: "Yes",
            okType: 'button-assertive'

        });
        confirmDel.then(function(response){
            if (response) {
                var index = $scope.syncList.indexOf(item);
                $scope.syncList.splice(index, 1);
                $localStorage.spots = $scope.syncList;
                //$localStorage.savedresults.splice($localStorage.savedresults.length-1-search_id,1);
                $state.go($state.current);
            }else{
                // don't delete
            }
        });

    };

    $scope.reloadData = function(){
        $scope.allList = $localStorage.spots;
        $scope.syncList = $filter('filter')($scope.allList, {syncStatus: false});
        //$localStorage.spots = $scope.syncList;
        for(i=0; i<$scope.syncList.length; i++){
            $scope.syncList[i].Selected = false;
            //$scope.selectedAll = false;
        }
    };

    $scope.createFile = function() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("nLocate",{create: true}, gotDir)
            //fileSystem.root.getFile("nLocateSpots.txt", {create: true}, gotFileEntry, fail);
        }

        function gotDir(dirEntry) {
            dirEntry.getFile("nLocateSpots.txt", {create: true}, gotFileEntry, fail);
        }

        function gotFileEntry(fileEntry) {
            fileEntry.createWriter(gotFileWriter, fail);
        }

        function gotFileWriter(writer) {
            writer.onwriteend = function (evt) {
                console.log("write success");
                $timeout(function(){
                    $scope.writeMsg = "Data saved to file nLocateSpots.txt";
                }, 3000);

            };
            writer.write($scope.syncList);
            writer.abort();
        }

        function fail(error) {
            console.log("error : " + error.code);
            $scope.writeMsg = "Some error occurred";

        }
    }

    //$scope.createZip = function(){
    //
    //    var PathToFileInString  = cordova.file.externalRootDirectory+"nLocate",
    //        PathToResultZip     = cordova.file.externalRootDirectory;
    //    JJzip.zip(PathToFileInString, {target:PathToResultZip,name:"SuperZip"},function(data){
    //        console.log('zip bhayo');
    //        /* Wow everiting goes good, but just in case verify data.success*/
    //    },function(error){
    //        console.log('zip bhaena');
    //        /* Wow something goes wrong, check the error.message */
    //    })
    //}
};

nLocateSpots.controller('ListController',ListController);
