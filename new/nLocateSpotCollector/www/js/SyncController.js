
SyncController = function($location, $scope, $state, $localStorage, $ionicModal, $rootScope,  $filter, $ionicPopup,$http, $window, $ionicNavBarDelegate,$timeout ) {

    $scope.apiAddress= "https://admin.nlocate.com/api";
    //$scope.apiAddress= "http://192.168.10.118:8089/api";
    $scope.allList = $localStorage.spots;

    $scope.syncList = $filter('filter')($scope.allList, {syncStatus: false});

    $scope.callTimeOut = function(i, x){
        $scope.checkTimeout = $timeout(function(){
            if(!$scope.syncList[i].syncStatus && !$scope.syncList[i].errMsg ){
                $scope.syncList[i].syncStart = false;
                $scope.syncList[i].errMsg = "Spot not synced.";
                $scope.$apply();
                $scope.fatafatSync(i+=1);
            }
        }, x*60000, true, i);
    };


    $scope.reloadData = function(){
        $rootScope.allList = $localStorage.spots;
        $scope.syncList = $filter('filter')($localStorage.spots, {syncStatus: false});
        $rootScope.selectedAll = false;
        $scope.total = '';
        for(i=0; i<$scope.syncList.length; i++) {
            $scope.syncList[i].Selected = false;
            $scope.syncList[i].syncStart = false;
            $scope.syncList[i].syncLog = '';
            $scope.syncList[i].errMsg = false;
            $scope.syncList[i].logDetails = false;
            //$timeout.cancel($scope.callTimeOut(i, 2));

            if ($scope.syncList[i].image && $scope.syncList[i].image.length >= 1) {
                for(j=0; j< $scope.syncList[i].image.length; j++){
                    $scope.syncList[i].image[j].uploadingImage = false;
                    //$scope.syncList[i].image[j].uploadedImage = false;
                    $scope.syncList[i].image[j].uploadError = false;
                }
            }
        }
    };

    $scope.reloadData();

    //watch check
    $scope.$watch('syncList', function() {
        $scope.total = $filter('filter')($scope.syncList, {Selected: true}).length;
    }, true);
    //select all

    $scope.checkAll = function () {

        if (!$rootScope.selectedAll) {
            $rootScope.selectedAll = true;
            $scope.total = $scope.syncList.length;

        } else {
            $rootScope.selectedAll = false;
            $scope.total = '';
        }

        angular.forEach($scope.syncList, function (spot) {
            spot.Selected = $rootScope.selectedAll;
        });

    };

    $rootScope.startSync = function(){
        if(!$rootScope.loggedIn){
            $scope.showPopup();
        }
        else if ($rootScope.loggedIn){
            $scope.syncData();
        }
    };

    $scope.freshList = $localStorage.spots;
    $scope.syncingList = $filter('filter')($scope.freshList, {Selected: true});

    $scope.showPopup = function() {
        $scope.data = {};
        $scope.setDefault = function () {
            console.log('Default set', arguments);
            $scope.$onClose({ test: 'hello' });
        };

        $scope.user = {};
        $ionicPopup.show({
            template: '',
            title: 'Login Now',
            scope: $scope,
            template: '<div style="color: red;" ng-if="loginError">Invalid username or password. Try again.</div> <input type="text" ng-model="user.name" placeholder=" Username"><br/><input type="password" ng-model="user.password" placeholder=" Password">',
            buttons: [
                {
                    text: 'Cancel', onTap: function(e) { return 'close';  }
                },
                { text: 'Login', type: 'button-positive', onTap: function(e) { return 'login'; } },

            ]
        }).then(function(res) {
            console.log('Tapped!', res);
            if(res == 'close'){
                $state.go('sync');
            }
            else if(res == 'login'){
                $scope.login($scope.user.name, $scope.user.password);
            }
        }, function(err) {
            console.log('Err:', err);
        }, function(msg) {
            console.log('message:', msg);
        });
    };

    //login
    $scope.login = function(username, password) {

        $scope.loginApi = $scope.apiAddress+"/login/";
        var load = {username:username, password:password};
        var request = {
            method : 'POST',
            url : $scope.loginApi,
            data : load
        };
        // $http returns a promise, which has a then function, which also returns a promise
        $http.post(request.url, load ).
            success(function(response) {
                console.log(response);
                if(response.ok == true){
                    $scope.loginError = false;
                    $rootScope.loggedIn = true;
                    $localStorage.loggedIn = true;
                    $localStorage.username = username;
                    $localStorage.password = password;
                    $scope.syncData();
                }
                else{
                    $scope.loginError = true;
                    $rootScope.loggedIn = false;
                    $scope.showPopup();
                }
            }).
            error(function(response) {
                console.log(response);
            });
    };


    $scope.syncData = function(){
        $scope.username = $localStorage.username;
        $scope.password = $localStorage.password;

        $scope.fatafatSync = function(i){
            if(i<$scope.syncList.length) {
            console.log("Syncing Spot: "+i);
            $scope.spotApi = $scope.apiAddress+"/add-spot/";
            $scope.imageAPI = $scope.apiAddress+"/image/";
            var spot = {username: $scope.username, password: $scope.password, index: i, data:$scope.syncList[i] };
            var request = {
                method : 'POST',
                url : $scope.spotApi,
                data : spot
            };

            // $http returns a promise, which has a then function, which also returns a promise

                if ($scope.syncList[i].Selected == true) {
                    $scope.syncList[i].syncStart = true;
                    $scope.syncList[i].errMsg = '';
                    $scope.syncList[i].syncLog = [];
                    $http.post(request.url, spot).
                        success(function (response) {
                            console.log(response);
                            response.index = parseInt(response.index);
                            console.log('response.index '+response.index);
                            if (response.ok == true) {
                                $scope.syncList[i].syncLog.push('New spot created.');
                                $scope.syncList[response.index].spotId = response.id;
                                //console.log($scope.syncList[response.index].image.length);
                                //if ($scope.syncList[response.index].image) {
                                    if ($scope.syncList[response.index].image && $scope.syncList[response.index].image.length >= 1) {
                                        var x = $scope.syncList[response.index].image.length;

                                        $scope.uploadPhoto = function (j) {

                                            if (j < $scope.syncList[response.index].image.length) {
                                                if(!$scope.syncList[i].image[j].uploadedImage) {
                                                    //$scope.callTimeOut(i);
                                                    //$scope.uploadPhoto = function(imageURI, j, i , id ) {
                                                    $scope.syncList[i].image[j].uploadingImage = true;
                                                    //$scope.$apply();
                                                    var imageURI = $scope.syncList[response.index].image[j].name;
                                                    var id = response.id;
                                                    //i = parseInt(i);
                                                    //j = parseInt(j);
                                                    console.log('image ' + j + 'uploading');
                                                    $scope.syncList[i].syncLog.push('Uploading image: ' + j);
                                                    //console.log(imageURI);
                                                    var options = new FileUploadOptions();
                                                    options.fileKey = "image";
                                                    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                                                    options.mimeType = "image/jpeg";
                                                    var params = new Object();
                                                    params.username = $scope.username;
                                                    params.password = $scope.password;
                                                    params.index = i;
                                                    params.id = id;
                                                    options.params = params;
                                                    options.chunkedMode = false;

                                                    var ft = new FileTransfer();
                                                    ft.upload(imageURI, $scope.imageAPI, win, fail, options);

                                                    function win(r) {
                                                        //console.log("Code = " + r.responseCode);
                                                        //console.log("Response = " + r.response);
                                                        //console.log("Sent =  image " + i +"ko "+ r.bytesSent);
                                                        //console.log(i + ' uploaded');
                                                        //$scope.uploadStatus = r.bytesSent +" bytes of image uploaded.";
                                                        console.log('image' + j + 'uploaded');
                                                        $scope.syncList[i].syncLog.push('Upload success.');
                                                        $scope.syncList[i].image[j].uploadedImage = true;
                                                        $scope.syncList[i].image[j].uploadingImage = false;
                                                        //$timeout.cancel($scope.callTimeOut(i, x));
                                                        $scope.$apply();
                                                        //$scope.syncList[i].msg =
                                                        $scope.uploadPhoto(j += 1);
                                                    }

                                                    function fail(error) {
                                                        console.log("An error has occurred: Code = " + error.code + 'for image' + j);
                                                        $scope.syncList[i].syncLog.push('Upload error. Error code = ' + error.code+ ' for image '+j);
                                                        $scope.syncList[i].image[j].uploadError = true;
                                                        $scope.syncList[i].image[j].uploadingImage = false;
                                                        $scope.syncList[i].errMsg = "Sync Error. Try again later";
                                                        $scope.uploadPhoto(j += 1);
                                                        //$timeout.cancel($scope.callTimeOut(i, x));
                                                        $scope.$apply();
                                                    }
                                                }
                                                else{
                                                    $scope.uploadPhoto(j+=1);
                                                }
                                            }
                                            else {
                                                //console.log('image sakie');
                                                $scope.syncList[i].syncLog.push('Done with this spot.');
                                                console.log($scope.syncList[i].syncLog);
                                                $scope.syncList[i].error = false;
                                                for (j = 0; j < $scope.syncList[i].image.length; j++) {
                                                    console.log($scope.syncList[i].image[j].uploadedImage);
                                                    if ($scope.syncList[i].image[j].uploadedImage == false) {
                                                        $scope.syncList[i].error = true;
                                                        $scope.$apply();
                                                    }
                                                }
                                                console.log($scope.syncList[i].error);
                                                if ($scope.syncList[i].error) {
                                                    $scope.fatafatSync(i += 1);
                                                }
                                                else {
                                                    $scope.changeStatus(i);
                                                }
                                            }
                                        };
                                        $scope.uploadPhoto(0);
                                    }
                                //}
                                else {
                                    $scope.syncList[i].syncLog.push('No images in this spot.');
                                    $scope.syncList[i].syncLog.push('Done with this spot.');
                                    $scope.syncList[response.index].syncStatus = true;
                                    $scope.syncList[response.index].Selected = false;
                                    $scope.syncList[response.index].syncStart = false;
                                    $scope.fatafatSync(i += 1);
                                }
                            }
                            else if (response.ok == false) {
                                $scope.syncList[i].syncLog.push('Received ok false from server.');
                                $scope.syncList[i].syncLog.push('Done with this spot');
                                $scope.syncList[i].syncStart = false;
                                $scope.syncList[i].errMsg = "Spot not synced.";
                                $scope.fatafatSync(i += 1);
                            }

                        }).
                        error(function (response) {
                            console.log(response);
                            $scope.syncList[i].syncLog.push('Request Error.');
                            $scope.syncList[i].syncLog.push('Done with this spot');
                            $scope.syncList[i].errMsg = response;
                            $scope.syncList[i].syncStart = false;
                            $scope.fatafatSync(i += 1);
                        });
                }
                else{
                    $scope.fatafatSync(i += 1);
                }
            }
            else{

                $scope.done();
            }

        };


        $scope.fatafatSync(0);
    };

    $scope.done = function(){
        //$scope.changeStatus(i);
        console.log('done');
    };

    $scope.changeStatus = function(i){
        $scope.syncList[i].syncStart = false;
        $scope.syncList[i].Selected = false;
        $scope.syncList[i].syncStatus = true;
        $scope.fatafatSync(i+=1);
        $scope.$apply();
    };

    $scope.cancelSync = function(){
        var confirmCancel = $ionicPopup.confirm({
            title: 'DANGER !',
            template: "Your spots are being uploaded. Sure you want to cancel?",
            cancelText: 'Nope!',
            cancelType: 'button-positive',
            okText: "Yes",
            okType: 'button-assertive'

        });
        confirmCancel.then(function(response){
            if (response) {
                $state.go('sync', {}, {reload: true});
            }else{
                // don't delete
            }
        });

    };

    $scope.showLog = function(i){
        if(!$scope.syncList[i].logDetails)
        {$scope.syncList[i].logDetails = true;}
        else{$scope.syncList[i].logDetails = false;}
    }

};

nLocateSpots.controller('SyncController',SyncController);