nLocateSpots.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('form', {
            url: "/",
            views: {
                'bodyContent': {
                    templateUrl: "templates/form1.html",
                    controller : "FormController"
                }
            }
        })
        .state('form2', {
            url: "/form2",
            //parent: 'form',
            views: {
                'bodyContent' : {
                    templateUrl: "templates/form2.html",
                    controller: "FormController"
                }
            }
        })
        .state('form3', {
            url: "/form3",

            views: {
                'bodyContent' : {
                    templateUrl: "templates/form3.html",
                    controller: "FormController"
                }
            }
        })
        .state('form4', {
            url: "/form4",

            views: {
                'bodyContent' : {
                    templateUrl: "templates/form4.html",
                    controller: "FormController"
                }
            }
        })

        .state('review', {
            url: "/review",

            views: {
                'bodyContent' : {
                    templateUrl: "templates/review.html",
                    controller: "FormController"
                }
            }
        })
        .state('picture', {
            url: "/picture",
            views: {
                'bodyContent' : {
                    templateUrl: "templates/picture.html",
                    controller: "FormController"
                }
            }
        })

        .state('spotList', {
            url: "/spotList",

            views: {
                'bodyContent' : {
                    templateUrl: "templates/spotList.html",
                    controller: "ListController"
                }
            }
        })

        .state('sync', {
            url: "/sync",

            views: {
                'bodyContent' : {
                    templateUrl: "templates/sync.html",
                    controller: "SyncController"
                }
            }
        })

        .state('syncProgress', {
            url: "/syncProgress",

            views: {
                'bodyContent' : {
                    templateUrl: "templates/syncprogress.html",
                    controller: "SyncProcessController"
                }
            }
        })

        .state('options',{
        url: "/options",

        views: {
            'bodyContent': {
                templateUrl : "templates/options.html",
                controller:"ListController"
            }
        }
    })
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');
});
//Routes
