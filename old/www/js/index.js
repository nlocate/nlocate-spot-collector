/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        var imageInfo = [];

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {

        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    clear: function(){
            window.localStorage.clear();
            alert("All cleared");
    },

    takePicture: function() {
        var lat, long, imageURI;
        var entry = JSON.parse(window.localStorage.getItem("entry")) || {};
        //var entry={};

        function afterPositionAcquired(position) {
            lat = position.coords.latitude;
            long = position.coords.longitude;
            var location = {
                "latitude": lat,
                "longitude": long
            };
            var newimageURI = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            //alert(newimageURI);
            try {
                entry[newimageURI] = location;
                window.localStorage.setItem("entry", JSON.stringify(entry));
                //alert(JSON.parse(window.localStorage.getItem("entry")));
            }catch(e){
                alert(e);
            }
            //movePic(imageURI);

            var gotFileEntry = function(fileEntry) {
                //alert("got image file entry: " + fileEntry.fullPath);
                //alert(lat+','+long);
                var name="Lat-"+lat+"-Long-"+long+"-.jpg";
                var gotFileSystem = function(fileSystem) {

                    fileSystem.root.getDirectory("DCIM/nLocate", {
                        create : true
                    }, function(dataDir) {

                        // copy the file
                        fileEntry.moveTo(dataDir, name, null, fsFail);

                    }, dirFail);

                };
                // get file system to copy or move image file to
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem,
                    fsFail);
            };
            // resolve file system for image
            window.resolveLocalFileSystemURI(imageURI, gotFileEntry, fsFail);

            // file system fail
            var fsFail = function(error) {
                alert("failed with error code: " + error.code);

            };

            var dirFail = function(error) {
                alert("Directory error code: " + error.code);

            };


            alert("Image captured and geolocation added!");
        }

        function onError(error) {
            alert('Please check your location settings and make sure they are turned on!');
        }
        //function movePic(file){
        //    window.resolveLocalFileSystemURI(file, resolveOnSuccess, resOnError);
        //}

//Callback function when the file system uri has been resolved
//        function resolveOnSuccess(entry){
//            var d = new Date();
//            var n = d.getTime();
//            var all = window.localStorage.getItem('entry');
//            var i;
//            var abc = JSON.parse(all);
//            //new file name
//            var newFileName = "Lat-"+abc.latitude+"Long-"+abc.longitude+".jpg";
//            var myFolderApp = "DCIM/Camera";
//            alert(newFileName);
//            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {
//                    //The folder is created if doesn't exist
//                    fileSys.root.getDirectory( myFolderApp,
//                        {create:true, exclusive: false},
//                        function(directory) {
//                            entry.moveTo(directory, newFileName,  successMove, resOnError);
//                        },
//                        resOnError);
//                },
//                resOnError);
//        }
//
////Callback function when the file has been moved successfully - inserting the complete path
//        function successMove(entry) {
//            //I do my insert with "entry.fullPath" as for the path
//        }
//
//        function resOnError(error) {
//            alert("error 1 "+ error.code);
//        }

            function afterImageUriAcquired(uri) {
                imageURI = uri;
                //alert('image captured');
                    navigator.geolocation.getCurrentPosition(afterPositionAcquired, onError);

            }

          navigator.camera.getPicture( afterImageUriAcquired,function( message ) {
              alert(message);
          },
          {
            quality: 90,
            destinationType: Camera.DestinationType.FILE_URI,
            saveToPhotoAlbum: true
          });
    },

    getImage: function(){
        var count = 0;
        var i=0;
        var url = "192.168.2.12/nlocate/upload.php";
        window.imagePicker.getPictures(
            function(results) {
                for (var i = 0; i < results.length; i++) {
                    upload(results[i]);
                    //upload(results[i]);
                    function upload(imageURI) {
                        alert(imageURI);
                        var options = new FileUploadOptions();
                        options.fileKey = "file";
                        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                        options.mimeType = "image/jpeg";
                        var all = window.localStorage.getItem('entry');
                        var loc = imageURI.match(/([0-9]+)/g)[0] + ".jpg";
                        var abc = JSON.parse(all);
                        alert(options.fileName);
                        alert(abc);
                        //var newFileName = "Lat-"+abc.latitude+"Long-"+abc.longitude+".jpg";
                        //var noposlat = 27.5;
                        //var noposlong = 85.5;
                        var params = new Object();
                        params.value1 = abc[loc].latitude;
                        params.value2 = abc[loc].longitude;
                        alert(params.value1 + ',' + params.value2);
                        options.params = params;
                        options.chunkedMode = false;
                        var ft = new FileTransfer();
                        ft.upload(imageURI, "http://192.168.2.12/nlocate/upload.php", win, fail, options);
                    }

                    function win(r) {
                        count++;
                        alert(r.response);
                        //document.getElementsByClassName('alert-message success').innerHTML = count + "images uploaded.";
                        //upload(results[i++]);
                        $('.alert-success').html(count + " images uploaded.");
                    }

                    //
                    function fail(error) {
                        alert("An error has occurred: Code = " + error.code);
                        $('.alert-success').html("An error has occurred. Please try again later.");
                    }

                }
            }, function (error) {
                alert('Error: ' + error);
            }
        );
        //alert(count +"/"+ i + "images uploaded");
        $('.alert-success').html("<img src='img/loader.gif' height='60px' width='60px' style='margin-left:-5px;'>Syncing with remote server. Please wait!");
    },

    viewRecords: function(){
        var entry = JSON.parse(window.localStorage.getItem("entry")) || {};
        var all = window.localStorage.getItem('entry');
        var abc = JSON.parse(all);
        alert('hello');
        var url="192.168.2.12";
        try {
            $('.ipaddress').html(url);
        }catch(e){
            alert(e);
        }
        try {
            $('.imgrecords').html(abc);
        }catch(e){
            alert(e);
        }
    }
};
