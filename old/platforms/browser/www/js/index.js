/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

//var lat;
//var long;
//var imageURI;
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        var imageInfo = [];
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    clear: function(){
            window.localStorage.clear();
            alert("All cleared");
    },

    takePicture: function() {
        var lat, long, imageURI;

        function afterPositionAcquired(position) {
            lat = position.coords.latitude;
            long = position.coords.longitude;
            //alert(lat+long);
            //alert(imageURI);
            var entry = {
                "title": imageURI,
                "latitude": lat,
                "longitude": long
            };
            //alert(entry+"title"+entry.title+ "Lat: "+entry.latitude+ "Long: "+ entry.longitude);
            window.localStorage.setItem("entry", JSON.stringify(entry));
            var allEntries = JSON.parse(window.localStorage.getItem("allEntries")) || [];
            allEntries.push(entry);
            window.localStorage.setItem("allEntries", JSON.stringify(allEntries));
            alert("Done");
        }

            function afterImageUriAcquired(uri) {
                imageURI = uri;
                navigator.geolocation.getCurrentPosition(afterPositionAcquired);
            }

          navigator.camera.getPicture( afterImageUriAcquired
          ,
          function( message ) {
              alert(message);
          },
          {
            quality: 90,
            destinationType: Camera.DestinationType.FILE_URI
          });
    },

    getFileSystem: function () {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) { // success get file system
            var sdcard = fileSystem.root;
            alert(sdcard);
            sdcard.getDirectory('DCIM', {create: false}, function (DCIM) {
                alert('dcim got');
                var gallery = $('#gallery');
                listDir(dcim, gallery);
            }, function (error) {
                alert(error.code);
            })
        }, function (evt) { // error get file system
            console.log(evt.target.error.code);
        });


        /* list on console the content of a directory*/
        function listDir(directoryEntry, domParent) {
            $.mobile.showPageLoadingMsg(); // show loading message

            var directoryReader = directoryEntry.createReader();

            directoryReader.readEntries(function (entries) { // success get files and folders
                for (var i = 0; i < entries.length; ++i) {
                    if (i % 2 == 0) domParent.append('<div class="ui-block-a"><div class="thumbnail"><img src="' + entries[i].fullPath + '" title="' + entries[i].name + '" /></div></div>');
                    else domParent.append('<div class="ui-block-b"><div class="thumbnail"><img src="' + entries[i].fullPath + '" title="' + entries[i].name + '" /></div></div>');
                    //console.log(entries[i].name);
                }
                $.mobile.hidePageLoadingMsg(); // hide loading message
            }, function (error) { // error get files and folders
                alert(error.code);
            });
        }
    },

        /* show an image */
        showImage:function () {
            var imgs = $('#gallery img');
            imgs.live('click', function () {
                var title = $(this).attr('title');
                $('#picture h1').text(title);
                $('#pic').html($(this).clone());

                $.mobile.changePage($('#picture'));
            });
        },



getImage: function() {
        //Retrieve image file location from specified source
        navigator.camera.getPicture(uploadPhoto, function (message) {
                alert('get picture failed');
            }, {
                quality: 30,
                targetHeight: 200,
                destinationType: navigator.camera.DestinationType.FILE_URI,
                sourceType: navigator.camera.PictureSourceType.SAVEDPHOTOALBUM
            }
        );

        function uploadPhoto(imageURI) {
            alert(imageURI);
            var img = $('<img/>');
            img.attr('src', imageURI);
            //var fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            //alert('filename');
            //$img.css({position: 'absolute', left: '0px', top: '-999999em', maxWidth: 'none', width: 'auto', height: 'auto'});
            //$img.bind('load', function() {
            //    var canvas = document.createElement("canvas");
            //    canvas.width = $img.width();
            //    canvas.height = $img.height();
            //    var ctx = canvas.getContext('2d');
            //    ctx.drawImage($img[0], 0, 0);
            //    var dataUri = canvas.toDataURL('image/png');
            //    $img.attr('src', 'data:image/png;base64,' + imageURI);
            //});
            //$img.bind('error', function() {
            //    alert('Couldnt convert photo to data URI');
            //});
            //$('.hello').append($img);
            img.appendTo('.hello');
            //alert(imageURI);

           // window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
             //   fileEntry.file(function(fileObj) {

                   // var fileName = fileObj.fullPath;

                    //now use the fileName in your method
                    //ft.upload(fileName ,serverURL + '/ajax.php?fname=appuploadspotimage'...);
                    alert("1");
            try {
                //var options = new FileUploadOptions(fileKey, fileName, mimeType, params);
                var fileKey = "file";
                var fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                var mimeType = "image/jpeg";
                var options = FileUploadOptions(fileKey, fileName, mimeType, params);
                alert(options.fileName);
            }catch (e){
                alert(e)
            }
                    var params = new Object();
                    params.value1 = "test";
                    params.value2 = "param";

                    options.params = params;
                    options.chunkedMode = false;
                    alert("2");
                    var ft = new FileTransfer();
                    var test = ft.upload(imageURI, "http://192.168.2.9/nlocate/upload.php", win, fail, options);
                    alert(test);
              //  });
           // });
        }
    function win(r) {
        alert("Code = " + r.responseCode+"Response = " + r.response+"Sent = " + r.bytesSent);
        //console.log("Response = " + r.response);
        //console.log("Sent = " + r.bytesSent);
        //alert(r.response);
    }

    function fail(error) {
        alert("An error has occurred: Code = " + error.code);
    }

    }

};
